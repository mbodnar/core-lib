package com.mbodnar.corelib.toolbar;

import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mbodnar.corelib.R;

import java.util.ArrayList;

/**
 * Created by MBodnar on 17.01.2017.
 */

public abstract class ToolbarFragment extends Fragment implements Toolbar.OnMenuItemClickListener {

    private final String BUNDLE_SEARCH_VIEW_STATE = "BUNDLE_SEARCH_VIEW_STATE";
    private final String BUNDLE_SEARCH_VIEW_STATE_TEXT = "BUNDLE_SEARCH_VIEW_STATE_TEXT";

    protected Toolbar mToolbar;
    protected boolean mShowBackArrow;
    private ViewGroup mContent;
    private View mEmptyMessageContainer;
    private TextView mEmptyMessageText;
    private SearchView mSearchView;
    private ViewGroup mLoaderContainer;

    private boolean mIsIconified;
//    private String mSearchText;
//    private boolean mAllowSearch = false;
    private final int CODE_PERMISSION_REQUEST = 1000;


    private View.OnClickListener mNavigationBackListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            onNavigationBackClick();

        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mShowBackArrow = true;
        setHasOptionsMenu(true);
    }

    protected final AppCompatActivity getSupportActivity() {
        return (AppCompatActivity) getActivity();
    }

    protected final <T extends ToolbarFragment> T getParrentToolbarFragment() {
        Fragment frg = getParentFragment();
        if (frg != null) {
            return (T) frg;
        } else {
            return null;
        }
    }

    @Nullable
    @Override
    public final View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(getLayout(), container, false);
        if (savedInstanceState != null) {
            mIsIconified = savedInstanceState.getBoolean(BUNDLE_SEARCH_VIEW_STATE);
        } else {
            mIsIconified = true;
        }
        mContent = root.findViewById(R.id.fragment_frame_layout);
        mLoaderContainer = root.findViewById(R.id.fragment_toolbar_loader_container);
        mEmptyMessageContainer = root.findViewById(R.id.fragment_toolbar_empty_message_container);
        mEmptyMessageText = root.findViewById(R.id.fragment_toolbar_empty_message_text);
        mContent.removeAllViews();
        mToolbar = root.findViewById(R.id.fragment_toolbar);
        mToolbar.setTitle(getActivity().getTitle());
        mToolbar.setNavigationOnClickListener(mShowBackArrow ? mNavigationBackListener : null);
        mToolbar.setNavigationIcon(mShowBackArrow ? getResources().getDrawable(getBackDrawable()) : null);
        setTitle(getSupportActivity().getTitle());
        View contentFragment = onCreateFragmentView(inflater, container, savedInstanceState);
        if (contentFragment != null) {
            mContent.addView(contentFragment);
        }
        mEmptyMessageText.setText(getEmptyTextMessage());
        showContent();
        return root;
    }

    protected int getBackDrawable() {
        return R.drawable.ic_action_arrow_back;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    protected int getLayout() {
        return R.layout.fragment_toolbar;
    }

    protected final void showContent() {
        mContent.setVisibility(View.VISIBLE);
        mLoaderContainer.setVisibility(View.GONE);
        mEmptyMessageContainer.setVisibility(View.GONE);
    }

    protected final void showEmptyMessage() {
        mEmptyMessageContainer.setVisibility(View.VISIBLE);
        mLoaderContainer.setVisibility(View.GONE);
        mContent.setVisibility(View.GONE);
    }

    protected final void showLoader() {
        mLoaderContainer.setVisibility(View.VISIBLE);
        mContent.setVisibility(View.GONE);
        mEmptyMessageContainer.setVisibility(View.GONE);
    }

    protected final void requestPermission(String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int state = ActivityCompat.checkSelfPermission(getContext(), permission);
            if (state != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[] {permission}, CODE_PERMISSION_REQUEST);
            }
        }
    }

    protected final void requestPermission(String... permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int size = permission.length;
            ArrayList<String> permissions = new ArrayList<>();
            for (int i = 0; i < size; i++) {
                int state = ActivityCompat.checkSelfPermission(getContext(), permission[i]);
                if (state == PackageManager.PERMISSION_DENIED) {
                    permissions.add(permission[i]);
                }
            }
            if (permissions.size() > 0) {
                requestPermissions(permissions.toArray(new String[permissions.size()]), CODE_PERMISSION_REQUEST);
            } else {
                onAllPermissionsGranted();
            }
        } else {
            onAllPermissionsGranted();
        }
    }

    protected void onAllPermissionsGranted() {

    }

    protected String getEmptyTextMessage() {
        return "";
    }

    protected void onNavigationBackClick() {
        Fragment parrent = getParentFragment();
        if (parrent != null && parrent instanceof ToolbarFragment) {
            boolean back = ((ToolbarFragment) parrent).onBackPress();
            if (back) {
                getActivity().onBackPressed();
            }
        } else {
            getActivity().onBackPressed();
        }
    }


    @Override
    public final void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu = mToolbar.getMenu();
        menu.clear();
//        inflater.inflate(R.menu.toolbar_base_menu, menu);
//        MenuItem searchItem = menu.findItem(R.id.toolbar_base_menu_search);
//
//        mSearchView = (SearchView) MenuItemCompat.getActionView(searchItem);
//
//        mSearchView.setSubmitButtonEnabled(true);
//        if (!mIsIconified) {
//            searchItem.expandActionView();
//            mSearchView.setQuery(mSearchText, false);
//        }
//        mSearchView.setOnCloseListener(new SearchView.OnCloseListener() {
//            @Override
//            public boolean onClose() {
//                onSearchReset();
//                return false;
//            }
//        });
//        MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {
//            @Override
//            public boolean onMenuItemActionExpand(MenuItem item) {
//                return true;
//            }
//
//            @Override
//            public boolean onMenuItemActionCollapse(MenuItem item) {
//                onSearchReset();
//                return true;
//            }
//        });
//        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//            @Override
//            public boolean onQueryTextSubmit(String query) {
//                onSearchText(query);
//                return true;
//            }
//
//            @Override
//            public boolean onQueryTextChange(String newText) {
//                return false;
//            }
//        });
        mToolbar.setOnMenuItemClickListener(this);
//        searchItem.setVisible(mAllowSearch);
        onCreateToolbarMenu(menu, inflater);
    }

    protected void setTitle(@StringRes int title) {
        mToolbar.setTitle(title);
    }

    protected void setTitle(CharSequence title) {
        mToolbar.setTitle(title);
    }

    public Toolbar getToolbar() {
        return mToolbar;
    }


    protected void onCreateToolbarMenu(Menu menu, MenuInflater inflater) {}

    protected void onPermissionGranted(String permission) {}
    protected void onPermissionDenied(String permission) {}

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == CODE_PERMISSION_REQUEST) {
            int size = grantResults.length;
            int granted = 0;
            for (int i = 0; i < size; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    onPermissionGranted(permissions[i]);
                    granted++;
                } else if(grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    onPermissionDenied(permissions[i]);
                }
                if (granted == size) {
                    onAllPermissionsGranted();
                }
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if (mSearchView != null) {
            mIsIconified = mSearchView.isIconified();
            outState.putString(BUNDLE_SEARCH_VIEW_STATE_TEXT, mSearchView.getQuery().toString());
        }
        outState.putBoolean(BUNDLE_SEARCH_VIEW_STATE, mIsIconified);
    }

    protected abstract  View onCreateFragmentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState);

    public boolean onBackPress() {
        return true;
    }
}
