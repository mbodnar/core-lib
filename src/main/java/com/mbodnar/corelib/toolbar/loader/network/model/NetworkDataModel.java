package com.mbodnar.corelib.toolbar.loader.network.model;

import android.content.Context;
import android.support.annotation.StringRes;

/**
 * Created by m.bodnar on 19.02.2018.
 */

public class NetworkDataModel {
    public static final int NO_ANY_RESPONSE = Integer.MIN_VALUE;
    private int mNetworkCode;
    private String mMessage;
    private Context mContext;



    public final void setNetworkCode(int networkCode) {
        mNetworkCode = networkCode;
        mMessage = decodeRespCode(mNetworkCode);
    }

    public final boolean isInformation() {
        return mNetworkCode >= 100 && mNetworkCode <= 199;
    }

    public final boolean isSuccess() {
        return mNetworkCode >= 200 && mNetworkCode <= 299;
    }

    public final boolean isRedirection() {
        return mNetworkCode >= 300 && mNetworkCode <= 399;
    }

    public final boolean isClientError() {
        return mNetworkCode >= 400 && mNetworkCode <= 499;
    }

    public final boolean isServerError() {
        return mNetworkCode >= 500 && mNetworkCode <= 599;
    }

    public final boolean hasInternetConnection() {
        return mNetworkCode != NO_ANY_RESPONSE;
    }

    public final int getNetworkCode() {
        return mNetworkCode;
    }

    protected String decodeNetworkResp(int networkCode) {
        switch (networkCode) {
            case 100:
                return "Continue";
            case 101:
                return "Switching Protocols";
            case 102:
                return "Processing";
            case 103:
                return "Early Hints";
            case 200:
                return "OK";
            case 201:
                return "Created";
            case 202:
                return "Accepted";
            case 203:
                return "Non-Authoritative Information";
            case 204:
                return "No Content";
            case 205:
                return "Reset Content";
            case 206:
                return "Partial Content";
            case 207:
                return "Multi-Status"; //WebDAV protocol only
            case 208:
                return "Already Reported"; //WebDAV protocol only
            case 226:
                return "IM Used";
            case 300:
                return "Multiple Choices";
            case 301:
                return "Moved Permanently";
            case 302:
                return "Found";
            case 303:
                return "See other";
            case 304:
                return "Not Modified";
            case 305:
                return "Use Proxy";
            case 306:
                return "Switch Proxy";
            case 307:
                return "Temporary Redirect";
            case 308:
                return "Permanent Redirect";
            case 400:
                return "Bad Request";
            case 401:
                return "Unauthorized";
            case 402:
                return "Payment Required";
            case 403:
                return "Forbidden";
            case 404:
                return "Not found";
            case 405:
                return "Method Not Allowed";
            case 406:
                return "Not Acceptable";
            case 407:
                return "Proxy Authentication Required";
            case 408:
                return "Request Timeout";
            case 409:
                return "Conflict";
            case 410:
                return "Gone";
            case 411:
                return "Length Required";
            case 412:
                return "Precondition Failed";
            case 413:
                return "Payload Too Large";
            case 414:
                return "URI Too Long";
            case 415:
                return "Unsupported Media Type";
            case 416:
                return "Range Not Satisfiable";
            case 417:
                return "Expectation Failed";
            case 418:
                return "I'm a teapot";
            case 420:
                return "Method Failure"; // Spring framework
            case 421:
                return "Misdirected Request";
            case 422:
                return "Unprocessable Entity";
            case 423:
                return "Locked";
            case 424:
                return "Failed Dependency";
            case 426:
                return "Upgrade Required";
            case 428:
                return "Precondition Required";
            case 429:
                return "Too Many Requests";
            case 431:
                return "Request Header Fields Too Large";
            case 440:
                return "Login Time-out";
            case 444:
                return "No Response";
            case 449:
                return "Retry With";
            case 450:
                return "Blocked by Windows Parental Controls";
            case 451:
                return "Unavailable For Legal Reasons";
            case 495:
                return "SSL Certificate Error";
            case 496:
                return "SSL Certificate Required";
            case 497:
                return "HTTP Request Sent to HTTPS Port";
            case 498:
                return "Invalid Token";
            case 499:
                return "Token Required";
            case 500:
                return "Internal Server Error";
            case 501:
                return "Not Implemented";
            case 502:
                return "Bad Gateway";
            case 503:
                return "Service Unavailable";
            case 504:
                return "Gateway Timeout";
            case 505:
                return "HTTP Version Not Supported";
            case 506:
                return "Variant Also Negotiates";
            case 507:
                return "Insufficient Storage";
            case 508:
                return "Loop Detected";
            case 509:
                return "Bandwidth Limit Exceeded";
            case 510:
                return "Not Extended";
            case 511:
                return "Network Authentication Required";
            case 520:
                return "Unknown Error";
            case 521:
                return "Web Server Is Down";
            case 522:
                return "Connection Timed Out";
            case 523:
                return "Origin Is Unreachable";
            case 524:
                return "A Timeout Occurred";
            case 525:
                return "SSL Handshake Failed";
            case 526:
                return "Invalid SSL Certificate";
            case 527:
                return "Railgun Error";
            case 530:
                return "Site is frozen";
            case 598:
                return "(Informal convention) Network read timeout error";
            default:
                return "Unknown problem";
        }
    }

    private String decodeRespCode(int networkCode) {
        switch (networkCode) {
            case NO_ANY_RESPONSE:
                return "No internet";
            default:
                return decodeNetworkResp(networkCode);
        }
    }


    private String decodeString(@StringRes int str_resource) {
        return getContext().getString(str_resource);
    }

    protected final Context getContext() {
        return mContext;
    }
}
