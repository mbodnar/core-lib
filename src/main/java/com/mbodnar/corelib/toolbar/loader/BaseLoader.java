package com.mbodnar.corelib.toolbar.loader;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

/**
 * Created by m.bodnar on 18.01.2018.
 */

public abstract class BaseLoader<D> extends AsyncTaskLoader<D> {
    private D mData;

    /**
     * Stores away the application context associated with context.
     * Since Loaders can be used across multiple activities it's dangerous to
     * store the context directly; always use {@link #getContext()} to retrieve
     * the Loader's Context, don't use the constructor argument directly.
     * The Context returned by {@link #getContext} is safe to use across
     * Activity instances.
     *
     * @param context used to retrieve the application context.
     */
    public BaseLoader(Context context) {
        super(context);
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        if (mData != null) {
            deliverResult(mData);
        }
        if (takeContentChanged() || mData == null) {
            forceLoad();
        }
    }

    @Override
    public void deliverResult(D data) {
        super.deliverResult(data);
        mData = data;
    }

    @Override
    protected void onReset() {
        super.onReset();
        if (isAbandoned()) {
            mData = null;
        }
    }
}
