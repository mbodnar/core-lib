package com.mbodnar.corelib.toolbar.loader.network;

import android.content.Context;
import android.os.Bundle;

import com.mbodnar.corelib.toolbar.loader.BaseLoader;
import com.mbodnar.corelib.toolbar.loader.network.model.NetworkDataModel;

import java.io.IOException;

/**
 * Created by m.bodnar on 19.02.2018.
 */

public abstract class NetworkLoader<Container extends NetworkDataModel> extends BaseLoader<Container> {
    private Bundle mArgs;
    public NetworkLoader(Context context, Bundle args) {
        super(context);
        mArgs = args;
    }

    @Override
    public final Container loadInBackground() {
        Container model = getModel();
        try {
            handleRequest(model, mArgs);
        } catch (IOException e) {
            model.setNetworkCode(NetworkDataModel.NO_ANY_RESPONSE);
            onHasError();
        }
        return model;
    }

    protected void onHasError() {

    }

    protected abstract void handleRequest(Container model, Bundle args) throws IOException;

    protected abstract Container getModel();
}
