package com.mbodnar.corelib.toolbar.masterdetail;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.mbodnar.corelib.toolbar.ToolbarLoaderFragment;

/**
 * Created by MBodnar on 11.12.2016.
 */

public abstract class DetailFragment<A extends DetailArgument> extends ToolbarLoaderFragment {

    public static final String BUNDLE_RUN_AS_MASTER = "DetailFragment.BUNDLE_RUN_AS_MASTER";

    private IDetail<A> mDetail;
    private MasterDetailFragment mMasterDetailAct;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof IMaster) {
//            mDetail = (IDetail) context;
//            mMasterDetailAct = (MasterDetailFragment) context;
//        }
        if(getParentFragment() != null && getParentFragment() instanceof IDetail) {
            mDetail = (IDetail) getParentFragment();
            mMasterDetailAct = (MasterDetailFragment) getParentFragment();
        } else {
            throw new IllegalArgumentException("The activity should implement IDetail interface");
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (mMasterDetailAct.isAllowTwoFragments() && mMasterDetailAct.getLayoutMode() != MasterDetailFragment.MODE_LAYOUT_DRAWER) {
            mToolbar.setNavigationOnClickListener(null);
            mToolbar.setNavigationIcon(null);
        }
        A arg = mDetail.getDetailArgument();
        if (arg != null && !TextUtils.isEmpty(arg.getTitle())) {
            setTitle(arg.getTitle());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getArguments().containsKey(BUNDLE_RUN_AS_MASTER) &&
                getArguments().getInt(BUNDLE_RUN_AS_MASTER) == 1) {
            updateData(mDetail.getDetailArgument());
        }
    }

    public void updateData(A argument) {
        initData(argument);
        setTitle(argument.getTitle());
    }

    abstract protected void initData(A argument);

    abstract protected void refreshData();

    protected final A getDetailArgument() {
        return mDetail.getDetailArgument();
    }

    public abstract void onDataReset();

    protected final void refreshMaster() {
        if (mDetail != null) {
            mDetail.refreshMaster();
        }
    }

    protected final void resetMaster() {
        if (mDetail != null) {
            mDetail.resetMaster();
        }
    }

    protected final void forceAutoSelect() {
        if (mDetail != null) {
            mDetail.forceAutoSelect();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mDetail = null;
        mMasterDetailAct = null;
    }

}
