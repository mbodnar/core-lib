package com.mbodnar.corelib.toolbar.masterdetail;

/**
 * Created by MBodnar on 11.12.2016.
 */

public interface IMaster<Arg extends DetailArgument> {
    boolean onSelectItem(Arg item);
    boolean onAutoSelect(Arg item);
    void refreshDetail();
    void onResetDetail();
}
