package com.mbodnar.corelib.toolbar.masterdetail;

import android.os.Parcelable;

/**
 * Created by MBodnar on 11.12.2016.
 */

public abstract class DetailArgument implements Parcelable {
    public static int POSITION_NOT_SELECTED = Integer.MIN_VALUE;

    abstract public String getTitle();
}
