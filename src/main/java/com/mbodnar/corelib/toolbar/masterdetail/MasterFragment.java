package com.mbodnar.corelib.toolbar.masterdetail;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.mbodnar.corelib.toolbar.ToolbarLoaderFragment;


/**
 * Created by MBodnar on 11.12.2016.
 */

public abstract class MasterFragment<Argument extends DetailArgument> extends ToolbarLoaderFragment {

    public static final String BUNDLE_IS_DETAIL_SHOVE = "BUNDLE_IS_DETAIL_SHOVE";

    private final String BUNDLE_ARGUMENT = "BUNDLE_ARGUMENT";

    private IMaster<Argument> mMaster;
    private MasterDetailFragment mMasterDetailFrg;
    private boolean mIsDetailShoved;
    protected Argument mLocalArgument;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            mLocalArgument = savedInstanceState.getParcelable(BUNDLE_ARGUMENT);
        }
        if (mLocalArgument == null) {
            mLocalArgument = createArgument();
        }
        mIsDetailShoved = getArguments().getBoolean(BUNDLE_IS_DETAIL_SHOVE, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (mMasterDetailFrg.isRunAsMaster() && mMasterDetailFrg.getLayoutMode() == MasterDetailFragment.MODE_LAYOUT_DRAWER) {
            mToolbar.setVisibility(View.GONE);
        }
        if (!mIsDetailShoved && mMasterDetailFrg.isAllowTwoFragments()) {
            onAutoSelectItem(getAutoSelectedItem());
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getParentFragment() != null && getParentFragment() instanceof IMaster)  {
            mMaster = (IMaster<Argument>) getParentFragment();
            mMasterDetailFrg = (MasterDetailFragment) getParentFragment();
        } else {
            throw new IllegalArgumentException("The activity/fragment should implement IMaster interface");
        }
    }


    public final void setSelectedItem(Argument item) {
        if (mMaster != null) {
            getArguments().putBoolean(BUNDLE_IS_DETAIL_SHOVE, mMaster.onSelectItem(item));
        }
    }

    protected final void onAutoSelectItem(Argument item) {
        if (mMaster != null) {
            getArguments().putBoolean(BUNDLE_IS_DETAIL_SHOVE, mMaster.onAutoSelect(item));
        }
    }

    protected abstract Argument createArgument();

    public final void refreshDetail() {
        if (mMaster != null) {
            mMaster.refreshDetail();
        }
    }

    public final void onResetDetailData() {
        if (mMaster != null) {
            mMaster.onResetDetail();
        }
    }

    abstract public void refreshData();
    abstract public void resetData();
    abstract public Argument getAutoSelectedItem();

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(BUNDLE_ARGUMENT, mLocalArgument);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMaster = null;
        mMasterDetailFrg = null;
    }
}
