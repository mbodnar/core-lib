package com.mbodnar.corelib.toolbar;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

import java.util.ArrayList;

/**
 * Created by m.bodnar on 18.01.2018.
 */

public abstract class ToolbarLoaderFragment extends ToolbarFragment implements LoaderManager.LoaderCallbacks {

    private ArrayList<Integer> mRunLoaders;
    private String BUNDLE_LOADERS = "BUNDLE_LOADERS";

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState == null) {
            mRunLoaders = new ArrayList<>();
        } else {
            mRunLoaders = savedInstanceState.getIntegerArrayList(BUNDLE_LOADERS);
            if (mRunLoaders != null) {
                for (Integer runLoader : mRunLoaders) {
                    LoaderManager loaderManager = getLoaderManager();
                    Loader loader = loaderManager.getLoader(runLoader);
                    if (loader != null && loader.isStarted() && !loader.isReset()) {
                        onLoaderDataLoading(runLoader, loader);
                        loaderManager.initLoader(runLoader, null, this);
                    }
                }
            } else {
                mRunLoaders = new ArrayList<>();
            }
        }
    }

    @Override
    public Loader onCreateLoader(int id, Bundle args) {
        Loader loader = createLoader(id, args);
        if (loader != null) {
            onLoaderDataLoading(id, loader);
            mRunLoaders.add(id);
        }
        return loader;
    }


    @Override
    public final void onLoadFinished(Loader loader, Object data) {
        onLoadDataFinished(loader, data);
    }

    private void removeLoader(int id) {
        if (mRunLoaders.contains(id)) {
            mRunLoaders.remove(mRunLoaders.indexOf(id));
        }
    }


    @Override
    public final void onLoaderReset(Loader loader) {
        removeLoader(loader.getId());
        onLoaderReseted(loader);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putIntegerArrayList(BUNDLE_LOADERS, mRunLoaders);
    }

    protected final void startLoader(int id,@Nullable Bundle args) {
        getLoaderManager().initLoader(id, args, this);
    }

    protected final void restartLoader(int id, @Nullable Bundle args) {
        getLoaderManager().restartLoader(id, args, this);
    }

    protected final void resetLoader(int id) {
        getLoaderManager().destroyLoader(id);
    }

    protected abstract Loader createLoader(int id, Bundle args);
    protected abstract void onLoadDataFinished(Loader loader, Object data);
    protected abstract void onLoaderReseted(Loader loader);
    protected abstract void onLoaderDataLoading(int id, Loader loader);
}
