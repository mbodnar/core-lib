package com.mbodnar.corelib.toolbar;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by m.bodnar on 01.02.2018.
 */

public abstract class BaseRecyclerAdapter<Data, Vh extends BaseRecyclerAdapter.BaseViewHolder> extends RecyclerView.Adapter<Vh> {

    private List<Data> mData;
    private Context mContext;
    private OnItemClick mClickListener;

    public BaseRecyclerAdapter(Context context, OnItemClick listener) {
        mContext = context;
        mData = new ArrayList<>();
        mClickListener = listener;
    }

    public Context getContext() {
        return mContext;
    }

    public Data getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mData.get(position).hashCode();
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    protected final OnItemClick getListener() {
        return mClickListener;
    }

    public void add(Data item) {
        mData.add(item);
    }

    public void add(Collection<Data> data) {
        mData.addAll(data);
    }

    public void setCollection(List<Data> dataList) {
        mData = dataList;
    }

    public void clear() {
        mData.clear();
    }

    public static class BaseViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener, View.OnLongClickListener {
        private OnItemClick mListener;

        public BaseViewHolder(View itemView, OnItemClick listener) {
            super(itemView);
            RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            itemView.setLayoutParams(lp);
            itemView.setOnClickListener(this);
            mListener = listener;
        }

        @Override
        public void onClick(View v) {
            mListener.onClick(this);
        }

        @Override
        public boolean onLongClick(View v) {
            return mListener.onLongClick(this);
        }
    }

    public interface OnItemClick {
        void onClick(RecyclerView.ViewHolder viewHolder);
        boolean onLongClick(RecyclerView.ViewHolder viewHolder);
    }

}
